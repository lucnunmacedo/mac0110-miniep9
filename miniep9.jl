using LinearAlgebra
using Test
function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dima[2]
				c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end
function matrix_pot(M,p)
	N=M
	for i in 1:(p-1)
		N=multiplica(N,M)
	end
	return N
end
function matrix_pot_by_squaring(M, p)
	N=M
	if(p%2==0)
		return matrix_pot_by_squaring(multiplica(N,N),p/2)
	else
		if(p!=1)
			return multiplica(N,matrix_pot_by_squaring(multiplica(N,N),(p-1)/2))
		else
			return N
		end
	end
end
function compare_times()
	M = Matrix(LinearAlgebra.I, 30, 30)
	@time matrix_pot(M, 10)
	@time matrix_pot_by_squaring(M, 10)
end
function test()
	@test matrix_pot([1 2 ; 3 4], 1) == ([1 2;3 4])
	@test matrix_pot([1 2 ; 3 4], 2) == ([7 10;15 22])
	@test matrix_pot([4 8 0 4 ; 8 4 9 6 ;9 6 4 0 ; 9 5 4 7], 3) == ([2756 2252 1476 1612; 3854 3314 2252 2466; 2718 2220 1648 1704 ; 3744 3113 2175 2411])
	@test matrix_pot_by_squaring([1 2 ; 3 4], 1) == ([1 2;3 4])
	@test matrix_pot_by_squaring([1 2 ; 3 4], 2) == ([7 10;15 22])
	@test matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ;9 6 4 0 ; 9 5 4 7], 3) == ([2756 2252 1476 1612; 3854 3314 2252 2466; 2718 2220 1648 1704 ; 3744 3113 2175 2411])
end
compare_times()